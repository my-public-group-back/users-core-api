import { AppModule } from '../src/app/app.module';
import { Test, TestingModule } from '@nestjs/testing';
import * as chai from 'chai';
import * as debug from 'debug';
import * as shortid from 'shortid';
import * as moment from 'moment';
import * as request from 'supertest';
import { User } from '../src/app/modules/user/user.entity';

const log: debug.IDebugger = debug('jolorenzom:user:e2e:spec');

describe('[API] User Endpoints (e2e)', () => {
  let app;
  const should: any = chai.should();
  let route: string;
  let createRoute: string;
  let getAllRoute: string;
  let getOneRoute: string;
  let updateRoute: string;
  let deleteRoute: string;
  let userCreated: User;
  const fqdn = shortid.generate().replace(/[_-]/g, '');

  const userData = {
    name: shortid.generate(),
    email: `${fqdn}@${fqdn}.com`,
    birthDate: moment().toDate(),
    address: {
      id: 1,
      street: shortid.generate(),
      state: shortid.generate(),
      city: shortid.generate(),
      country: shortid.generate(),
      zip: shortid.generate(),
    },
  };

  beforeAll(async () => {
    route = `/users`;
    createRoute = `/createUsers`;
    getAllRoute = `/getusers`;
    getOneRoute = `/getusersById`;
    updateRoute = `/updateUsersById`;
    deleteRoute = `/deleteUsersById`;

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  }, 10000);

  afterAll(async () => {
    if (app) {
      await app.close();
    }
  });

  it('Create a new user', done => {
    return request(app.getHttpServer())
      .post(route + createRoute)
      .send(userData)
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        userCreated = res.body;
        res.body.should.have.property('id', userCreated.id);
        res.body.should.have.property('name', userData.name);
        return done();
      });
  });

  it('Get a wrong user', done => {
    return request(app.getHttpServer())
      .get(`${route}${getOneRoute}${shortid.generate()}`)
      .expect(404)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('statusCode', 404);
        res.body.should.have.property('error', 'Not Found');
        return done();
      });
  });

  it('Get a user', done => {
    return request(app.getHttpServer())
      .get(`${route}${getOneRoute}/${userCreated.id}`)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.have.property('id', userCreated.id);
        return done();
      });
  });

  it('Get all user', done => {
    return request(app.getHttpServer())
      .get(route + getAllRoute)
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err);
        }
        res.body.should.be.instanceof(Array);
        res.body.should.have.length.above(1);
        return done();
      });
  });

  it('Update an user', done => {
    const userWithchanges = Object.assign({}, userData, {
      name: shortid.generate() + '_changed',
    });

    return request(app.getHttpServer())
      .put(`${route}${updateRoute}/${userCreated.id}`)
      .send(userWithchanges)
      .set('Content-Type', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .end((err, res) => {
        // if (err) { return done(err); }
        res.body.should.have.property('id', userCreated.id);
        res.body.should.have.property('name', userWithchanges.name);
        return done();
      });
  });

  // it('Delete an user', (done) => {
  //   return request(app.getHttpServer())
  //     .delete(`${route}${deleteRoute}/${userCreated.id}`)
  //     .expect(200)
  //     .end((err, res) => {
  //       if (err) { return done(err); }
  //       return done();
  //     });
  // });
  //
  // it('Get a removed user', (done) => {
  //   return request(app.getHttpServer())
  //     .get(`${route}${deleteRoute}/${shortid.generate()}`)
  //     .expect('Content-Type', /json/)
  //     .expect(404)
  //     .end((err, res) => {
  //       if (err) { return done(err); }
  //       res.body.should.have.property('statusCode', 404);
  //       res.body.should.have.property('error', 'Not Found');
  //       return done();
  //     });
  // });
});
