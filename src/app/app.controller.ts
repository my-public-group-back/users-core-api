import { Controller, Get } from '@nestjs/common';
import * as debug from 'debug';
import { AppService } from './app.service';

// Set debug
const log: debug.IDebugger = debug('jolorenzom:app:controller');

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
