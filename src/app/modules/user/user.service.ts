import {
  BadRequestException,
  HttpException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { User } from './user.entity';
import * as debug from 'debug';

const log: debug.IDebugger = debug('jolorenzom:user:service');

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async create(createUserDto): Promise<User> {
    return await this.userRepository.save(createUserDto);
  }

  async findAll(): Promise<User[]> {
    return await this.userRepository.find();
  }

  async findOne(id: number): Promise<User> {
    return await this.userRepository.findOne(id);
  }

  async update(id, updateUserDto): Promise<UpdateResult> {
    return await this.userRepository.update(id, updateUserDto);
  }

  async delete(id): Promise<DeleteResult> {
    return await this.userRepository.delete(id);
  }

  async checkCreateUser(createUserBody) {
    const duplicateEmail = await this.userRepository.findOne({
      email: createUserBody.email,
    });
    if (
      !createUserBody.name ||
      !createUserBody.email ||
      !createUserBody.birthDate ||
      !createUserBody.address ||
      duplicateEmail
    ) {
      throw new HttpException(
        { statusCode: 405, error: 'Invalid input', message: 'Invalid input' },
        405,
      );
    }
  }

  async checkIdFormat(id: number) {
    if (isNaN(id)) {
      throw new BadRequestException('Invalid user id');
    }
  }

  async checkGetUser(id) {
    const user = await this.findOne(id);
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }
}
