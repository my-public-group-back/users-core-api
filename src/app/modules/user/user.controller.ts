import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';
import * as debug from 'debug';

const log: debug.IDebugger = debug('jolorenzom:user:controller');

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('createUsers')
  async create(@Body() createUserBody): Promise<User> {
    await this.userService.checkCreateUser(createUserBody);
    return await this.userService.create(createUserBody);
  }

  @Get('getusers')
  async findAll(): Promise<User[]> {
    return await this.userService.findAll();
  }

  @Get('getusersById/:id')
  async findOne(@Param('id') id: number): Promise<User> {
    await this.userService.checkIdFormat(id);
    return await this.userService.checkGetUser(id);
  }

  @Put('updateUsersById/:id')
  async update(@Body() updateUserBody, @Param('id') id: number): Promise<any> {
    await this.userService.checkIdFormat(id);
    await this.userService.checkGetUser(id);
    await this.userService.update(id, updateUserBody);
    return await this.findOne(id);
  }

  @Delete('deleteUsersById/:id')
  async delete(@Param('id') id: number): Promise<any> {
    await this.userService.checkIdFormat(id);
    await this.userService.checkGetUser(id);
    const deletedUser = await this.userService.delete(id);
    return deletedUser.affected > 0 ? 'OK' : 'KO';
  }
}
