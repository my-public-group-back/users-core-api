import { Column } from 'typeorm';

export class Address {
  @Column({ type: 'int', unique: true, nullable: false })
  id: number;

  @Column({ type: 'varchar', unique: false, nullable: false })
  street: string;

  @Column({ type: 'varchar', unique: false, nullable: false })
  state: string;

  @Column({ type: 'varchar', unique: false, nullable: false })
  city: string;

  @Column({ type: 'varchar', unique: false, nullable: false })
  country: string;

  @Column({ type: 'varchar', unique: false, nullable: false })
  zip: string;
}
