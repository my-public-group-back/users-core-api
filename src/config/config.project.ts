export const config = {
  project: {
    name: 'Users API Core',
    description: 'Moveco API Core',
    poweredBy: 'jolorenzom',
    email: 'jolorenzom@gmail.com',
  },

  sites: {
    api: {
      port: 3001,
      url: 'localhost',
      protocol: 'http',
    },
  },

  databases: {
    mysqlProduction: {
      type: 'mysql',
      host: 'eu-cdbr-west-02.cleardb.net',
      port: 3306,
      username: 'b0c5807253025b',
      password: '0c623329',
      database: 'heroku_5814db1a23a9804',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      synchronize: true,
    },
    mysqlLocalhost: {
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'jolorenzom',
      password: 'admin',
      database: 'users-core-api-database',
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
      synchronize: true,
    },
  },
};
