import * as debug from 'debug';
import { merge } from 'lodash';
import { config as configBase } from './config.base';
import { config as configProject } from './config.project';

const log: debug.IDebugger = debug('jolorenzom:config');

// load environment configuration
let config: any = {
  envKey: process.env.ENV_KEY || 'jolorenzom',
  environment: process.env.NODE_ENV || 'development',
  isProduction: process.env.NODE_ENV === 'production',
  PWD: process.env.PWD,
};

config = merge(configBase, configProject, config);

export { config };
