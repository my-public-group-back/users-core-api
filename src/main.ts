import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { config } from './config/config';
import * as cors from 'cors';
import * as debug from 'debug';
import * as helmet from 'helmet';
import * as morgan from 'morgan';
import { ValidationPipe } from '@nestjs/common';

// Set debug
const log: debug.IDebugger = debug('jolorenzom:server');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Log every request to the console
  app.use(morgan(config.api.morgan));

  // Enable CORS middleware
  app.use(cors(config.api.cors));

  // Set Helmet
  app.use(helmet(config.api.helmet));

  await app.listen(process.env.PORT || config.sites.api.port);

  log(
    "%s's magic happens at %s://%s:%s \nPID: %s.\nEnvironment: %s.\nIs production: %s.\nRoot: %s",
    config.project.name,
    config.sites.api.protocol,
    config.sites.api.url,
    config.sites.api.port,
    process.pid || 'not forked',
    config.environment,
    config.isProduction,
    config.PWD,
  );
}
bootstrap();
